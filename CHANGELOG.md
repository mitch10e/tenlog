# TenLog CHANGELOG

## 0.2.2

###### Enhancements

- Added functionality to disable debug statements by tag

## 0.1.0

- Created initial library.